# octopi-notifier-frameworks

Notifier for Octopi with Knotifications support (for KDE)

https://tintaescura.com/projects/octopi/

https://github.com/aarnt/octopi

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/octopi/octopi-notifier-frameworks.git
```
